package main

import "fmt"
import "log"
import "os"
import "crypto/md5"
import "io"
import "encoding/hex"
import "os/exec"
import "bytes"

//find ./notes -type f  -exec md5sum {} + | awk '{print $2"-"$1}'

func main() {

	f, err := os.Open("main.go")
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()
	fileMd5 := fileMd5(f)
	fmt.Printf("[%s]\n ", fileMd5)

	copyFile(f.Name(), "/tmp/"+f.Name())

}

func copyFile(src string, sink string) {
	cmd := exec.Command("cp", src, sink)

	var stdOut bytes.Buffer
	var stdErr bytes.Buffer

	cmd.Stdout = &stdOut
	cmd.Stderr = &stdErr

	err := cmd.Run()

	if err != nil {
		log.Fatalf("error copying file stderr[%v] stdout[%v]\n", stdErr.String(), stdOut.String())
	}
}

func fileMd5(file *os.File) *md5File {

	h := md5.New()
	if _, err := io.Copy(h, file); err != nil {
		log.Fatal(err)
	}

	hashedFile := md5File{hex.EncodeToString(h.Sum(nil)), file.Name()}

	fmt.Printf("%s\n", hashedFile) //REMOVEME

	return &hashedFile
}

type md5File struct {
	hash     string
	fileName string
}
