package main

import (
	"fmt"
	"log"
	"os"
	"time"
)

func main() {

	flagFile := os.Args[1]

	now := time.Now().AddDate(0, 0, 30).Local()
	formattedTime := fmt.Sprintf("%d%02d%02d", now.Year(), now.Month(), now.Day())
	renameTo := fmt.Sprintf("%s.%s.rm", flagFile, formattedTime)
	err := os.Rename(flagFile, renameTo)

	if err != nil {
		log.Fatal(err)
	}
}
