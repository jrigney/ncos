package main

import (
	"io"
	"net/http"
	"os"
)

func main() {
	renameFile("sbt", "sbt.rm")
	sbtExtrasFile := "https://git.io/sbt"

	getSbt(sbtExtrasFile)

	//make sbt executable
	makeFileExecutable()

	removeFile("sbt.rm")
}

func getSbt(sbtExtrasFile string) {
	resp, _ := http.Get(sbtExtrasFile)
	defer resp.Body.Close()

	out, err := os.Create("sbt")
	if err != nil {
		panic(err)
	}
	defer out.Close()

	io.Copy(out, resp.Body)
}

func makeFileExecutable() {
	if err := os.Chmod("sbt", 0755); err != nil {
		panic(err)
	}

}

func renameFile(from string, to string) {
	if _, err := os.Stat(from); !os.IsNotExist(err) {
		if err := os.Rename(from, to); err != nil {
			panic(err)
		}
	}
}

func removeFile(file string) {
	if _, err := os.Stat(file); !os.IsNotExist(err) {
		if err := os.Remove(file); err != nil {
			panic(err)
		}
	}
}
